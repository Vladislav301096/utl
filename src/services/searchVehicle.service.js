import Axios from "axios";
//import queryString from "query-string";

export function searchAll() {
  Axios.get("/api/vehicles/all")
    .then((res) => {
      this.allVehicle = res.data;
      this.completed = !this.completed;
    })
    .catch((e) => console.log(e));
}
export function searchById() {
  if (this.vehicle) {
    Axios.get(`/api/vehicles/${this.vehicle}`)
      .then((res) => {

        this.results = res.data;
        this.completed = !this.completed;
      })
      .catch((e) => console.log(e));
  }
}

export function searchByPersonId() {
    Axios.get(`/api/vehicles/person/`)
}
