import Axios from "axios";

export function createPersonHandler() {
  const body = {
    passport: {
      endDate: this.model.passport.endDate,
      passportNumber: this.model.passport.passportNumber,
      startDate: this.model.passport.startDate,
      upassportId: Date.now(),
    },
    personName: this.model.personName,
    personSurname: this.model.personSurname,
    personPatronymic: this.model.personPatronymic,
  };
  //"http://localhost:8099/api/persons/
  Axios.post("https://jsonplaceholder.typicode.com/posts/", body)
    .then((res) => {
      console.log(JSON.stringify(res));
      this.$refs.form.reset();
    })
    .catch((e) => console.log(e));
}

export function updatePerson() {
  const body = {
    passport: {
      endDate: this.model.passport.endDate,
      passportNumber: this.model.passport.passportNumber,
      startDate: this.model.passport.startDate,
      upassportId: Date.now(),
    },
    personName: this.model.personName,
    personSurname: this.model.personSurname,
    personPatronymic: this.model.personPatronymic,
  };

  Axios.put(`https://api.github.com/user/ ${this.model.passport}`, body)
    .then((res) => {
      console.log(JSON.stringify(res));
      this.$refs.form.reset();
    })
    .catch((e) => console.log(e));
}
