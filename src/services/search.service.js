import Axios from "axios";
import queryString from "query-string";

export function searchAll() {
  Axios.get("https://jsonplaceholder.typicode.com/posts?_limit=5")
    .then((res) => {
      this.allPerson = res.data;
      this.completed = !this.completed;
    })
    .catch((e) => console.log(e));
}
export function searchById() {
  if (this.passport) {
    Axios.get(`https://api.github.com/user/ ${this.passport}`)
      .then((res) => {

        this.results = res.data;
        this.completed = !this.completed;
      })
      .catch((e) => console.log(e));
  }
}

export function searchByNSP() {
  const SEARCH_URL = `/api/persons/search/nsp?`;
  const searchParams = queryString.stringify({
    name: this.NSP.username,
    patronymic: this.NSP.patronymic,
    surname: this.NSP.patronymic,
  });

  Axios.get(`${SEARCH_URL}${searchParams}`)
    .then((res) => {
      this.results = res.data
      console.log(res.data);
      /* this.results.passports.passportNumber = res.data.passports.passportNumber
      this.results.passports.startDate = res.data.passports.startDate
      this.results.passports.upassportId = res.data.passports.upassportId
      this.results.username = res.data.personName;
      this.results.surname = res.data.personPatronymic;
      this.results.patronymic = res.data.personSurname; */
      
      // this.completed = !this.completed;
    })
    .catch((e) => console.log(e));
}

export function searchByPassportNum() {
  Axios.get(`/api/persons/search/passport/number?${this.passportNumber}`)
  .then((res) => {
    this.results = res.data
  })
  .catch((e) => console.log(e));
}

export function searchByUid() {
  Axios.get(`/api/persons/search/passport/uid?${this.upassportId}`)
  .then((res) => {
    this.results = res.data
  })
  .catch((e) => console.log(e));
}

export function searchByPasNumAndUid() {
  const SEARCH_URL = `api/persons/search/passport/numberanduid?`;
  const searchParams = queryString.stringify({
    number: this.passportNumber,
    uId: this.upassportId
  });
  Axios.get(`${SEARCH_URL}${searchParams}`)
  .then((res) => {
    this.results = res.data
  })
  .catch((e) => console.log(e));
}

