import Axios from "axios";

export function createVehicleHandler() {
  const body = {
    
      carBrand: this.model.carBrand,
      itemId: this.model.itemId,
      personId: this.model.personId,
      vinCode: this.model.vinCode,
    
  };

  Axios.post("https://jsonplaceholder.typicode.com/todos/", body)
  .then(
    (res) => {
      console.log(JSON.stringify(res));
      this.$refs.form.reset();
    }
  )
  .catch(e => console.log(e))
}
