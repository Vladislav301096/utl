import Vue from 'vue'
import Router from 'vue-router'
import SearchPage from "../components/SearchPersons/SearchPage"
import Person from '../components/Person'
import Vehicle from '../components/Vehicle'
import Login from '../components/Login'
import UpdatePerson from '../components/UpdatePerson'
import SearchVehicle from '../components/SearchVehicle/SearchVehicle'
Vue.use(Router)


export const router =  new Router({
    mode:'history',
    routes: [
        {
            path:'/',
            component: SearchPage,
        },
         {
            path:'/person',
            component: Person,
        },

        {
            path:'/vehicle',
            component:Vehicle,
        },

        {
            path:'/login',
            component: Login
        },

        {
            path:'/updatePerson',
            component: UpdatePerson
        },

        {
            path: '/searchVehicle',
            component: SearchVehicle
        }
        
    ]
})